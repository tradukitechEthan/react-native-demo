// Import library to create compenent
import React from 'react';
import { AppRegistry, View } from 'react-native';
import Header from './src/components/header';
import AlbumList from './src/components/AlbumList';

// Create component
const App = () => (
    <View>
        <Header headerText={'Voice to the Voiceless'} />
        <AlbumList />
    </View>
);

// Render component
AppRegistry.registerComponent('albums', () => App);
